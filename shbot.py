# Telegram Shell Bot
# Copyright (C) 2016-2017 Filippo Rigotto.
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of ShellBot nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Filippo Rigotto BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Crontab: @reboot sleep 30; su (user) -c 'python (dir)/shbot.py >> (dir)/shbot.log 2&>1 &'
# Dependencies: telepot
import  os, signal, sys, time, telepot

# EDIT these values
token = ''
myId = 000

noAuth = 'Not Authorized!'
startStr = 'This Telegram Bot executes shell commands on the Raspberry Pi where it\'s running.'
cmdStr = 'List of available commands:\n \
/stat - Get system\'s statistics\n \
/id - Show the user\'s id\n \
/reboot - Reboot the system\n \
/shutdown - Shutdown the system'

def trySend(id,msg):    #wrapping for logging
    try:
        print "S | {0} | {1}".format(id,msg.replace('\n',' ')[:20])
        bot.sendMessage(id,msg)
    except Exception as exc:
        print "E | {0} | {1} | {2}".format(id,msg[:20],exc)

def handle(msg):
    chatId = msg['chat']['id']
    text = msg['text'].strip()
    print "R | {0} | {1}".format(chatId,text)

    # DECODE COMMANDS

    cmd = ''
    arg = ''
    if text.startswith('/') and ' ' in text:
        spl = text.split(' ',1)
        cmd = spl[0]    #/cmd arg...
        arg = spl[1]
    elif text.startswith('/'):
        cmd = text      #/cmd

    # EXECUTE COMMANDS

    if cmd == '/start':
    	trySend(chatId,startStr)
    elif cmd == '/help':
    	trySend(chatId,cmdStr)

    elif cmd == '/stat':
        arr = filter(None, os.popen("df -h | grep /dev/root").read().split(' '))
        disk = "Used {2} - {1} of {0}\n".format(arr[1],arr[2],arr[4])

        arr = os.popen("vcgencmd measure_clock arm").read().split('=')
        freq = "{0}MHz".format(int(arr[1])/1000000)

        arr = os.popen("vcgencmd measure_temp").read().split('=')
        temp = arr[1]

        since = os.popen('uptime -p').read().replace('up ','')

        total = "Disk\n{0}\nCpu\n{1}   {2}\nUptime\n{3}\n".format(disk,freq,temp,since)
        if chatId == myId:
            total = "{0}\n{1}".format(total,pub)

        trySend(chatId,total)

    elif cmd == '/id':
    	trySend(chatId,"Id = {0}".format(chatId))

    elif cmd == '/reboot':
        trySend(chatId,noAuth)
        if chatId != myId:
            trySend(chatId,noAuth)
        else:
            trySend(myId,'Rebooting...')
            os.system("sudo reboot")
    elif cmd == '/shutdown':
        if chatId != myId:
            trySend(chatId,noAuth)
        else:
            trySend(myId,'Shutting down...')
            os.system("sudo poweroff")

    elif text != '':
        if chatId != myId:
            trySend(chatId,noAuth)
        else:
            res = os.popen(text).read()
            if len(res) > 0:
                trySend(myId,res)

def sigHdlr(sig,frame): #notify, log and safely shut down script
    sys.exit(0)

# MAIN PROGRAM

bot = None  #attempt to initialize w/o network errors
while bot == None:
    try:
        bot = telepot.Bot(token)
    except:
        bot = None
        time.sleep(5)

for sig in [signal.SIGINT,signal.SIGHUP,signal.SIGQUIT,signal.SIGTERM]:
    signal.signal(sig,sigHdlr)  #attaching signals

bot.message_loop(handle, run_forever=True)
#calls getUpdate every 0.1 seconds and msg handling if necessary
