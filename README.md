# ShellBot #

A Telegram bot that exposes an emergency shell for the Raspberry Pi / Linux system where it resides

### Dependencies ###

* [Telepot](https://github.com/nickoala/telepot)  
  Install using `sudo pip install telepot`

### Setup ###

* Download dependencies and script
* Create a telegram bot chatting *[@botfather](https://telegram.me/BotFather)*
* Set correct values for variable in the first part of the script
* Test-run it with `python shbot.py`
* If it works, set it to autostart at boot.

### AutoStart ###

* Type `crontab -e`
* change *(user)* with your current user,  
 *(dir)* with the absolute path of the script's directory
* add at the end `@reboot sleep 30; su (user) -c 'python (dir)/shbot.py >> (dir)/shbot.log 2&>1 &'`
* Save and exit
* Reboot the system

------------

Un bot Telegram che espone un terminale di emergenza per il Raspberry Pi o il PC Linux dove risiede

### Dipendenze ###

* [Telepot](https://github.com/nickoala/telepot)  
  Si installa con `sudo pip install telepot`

### Setup ###

* Scaricare le dipendenze e lo script
* Creare un bot telegram chattando con *[@botfather](https://telegram.me/BotFather)*
* Impostare i valori corretti per le variabili nella prima parte dello script
* Testare con `python shbot.py`
* Se funziona, impostarlo perchè parta all'avvio.

### AutoStart ###

* Digitare `crontab -e`
* cambiare *(user)* con l'utente corrente,  
 *(dir)* con il percorso assoluto della cartella dello script
* aggiungere alla fine `@reboot sleep 30; su (user) -c 'python (dir)/shbot.py >> (dir)/shbot.log 2&>1 &'`
* Salvare e uscire
* Riavviare il sistema

------------

### BSD-new License ###

Copyright (C) 2016-2017, Filippo Rigotto.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,  
  this list of conditions and the following disclaimer.  
* Redistributions in binary form must reproduce the above copyright notice,  
  this list of conditions and the following disclaimer in the documentation and/or  
  other materials provided with the distribution.  
* Neither the name of ShellBot nor the names of its contributors may be used  
  to endorse or promote products derived from this software without specific prior written permission.  

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND  
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED  
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  
DISCLAIMED. IN NO EVENT SHALL Filippo Rigotto BE LIABLE FOR ANY  
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES  
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;  
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND  
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT  
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS  
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
